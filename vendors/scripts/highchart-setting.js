// chart 1

Highcharts.chart('chart1', {
	chart: {
	  type: 'line'
	},
	title: {
	  text: 'Evolution du taux de clients'
	},
	subtitle: {
	  text: 'en fonction de conversion prospects/clients'
	},
	xAxis: {
	  categories: ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Decembre']
	},
		yAxis: [{ // left y axis
		  title: {
			  text: 'Nombre de dossiers'
		  },
		  labels: {
			  align: 'left',
			  x: 3,
			  y: 16,
			  format: '{value:.,0f}'
		  },
		  showFirstLabel: false
	  }],
	plotOptions: {
	  line: {
		dataLabels: {
		  enabled: true
		},
		enableMouseTracking: false
	  }
	},
	series: [{
	  name: 'Clients',
	  data: [30, 60,111, 97, 104,82, 115, 80, 170, 205, 220,250]
	}, {
	  name: 'Prospects',
	  data: [39, 42, 57, 85, 119, 152, 170, 166, 140, 135, 155, 202]
	}]
  });