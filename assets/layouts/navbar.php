    <?php if (!isset($_SESSION['auth'])) { ?>

        <div class="login-header box-shadow">
            <div class="container-fluid d-flex justify-content-between align-items-center">
                <div class="brand-logo">
                    <a href="../login/">
                        <img src="../assets/images/adexca-dark-logo.svg" alt="">
                    </a>
                </div>
            </div>
        </div>

        <?php } else { ?>
        <div class="header">
            <div class="header-left">
                <div class="menu-icon dw dw-menu"></div>
                <div class="search-toggle-icon dw dw-search2" data-toggle="header_search"></div>
                <div class="header-search">
                    <form>
                        <div class="form-group mb-0">
                            <i class="dw dw-search2 search-icon"></i>
                            <input type="text" class="form-control search-input" placeholder="Recherche">
                            <div class="dropdown">
                                <a class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
                                    <i class="ion-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="header-right">
                <div class="user-info-dropdown">
                    <div class="dropdown">
                        <a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon">
							<img src="../assets/uploads/users/<?php echo $_SESSION['profile_image']; ?>" alt="..." width="130" class="mb-2 img-thumbnail">
						</span>
                            <span class="user-name"><h6 class="mb-0 text-dark lh-100">Bonjour, <?php echo $_SESSION['first_name']; ?></h6></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                            <a class="dropdown-item" href="../profile/"><i class="dw dw-user1"></i> Profil</a>
                            <a class="dropdown-item" href="../compte/"><i class="dw dw-settings2"></i> paramètres</a>
                            <a class="dropdown-item" href="../logout/"><i class="dw dw-logout"></i> Déconnexion</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <div class="left-side-bar">
                <div class="brand-logo">
                    <a href="../home/">
                        <img src="../vendors/images/adexca-logo.svg" alt="" class="dark-logo">
                        <img src="../vendors/images/adexca-logo.svg" alt="" class="light-logo">
                    </a>
                    <div class="close-sidebar" data-toggle="left-sidebar-close">
                        <i class="ion-close-round"></i>
                    </div>
                </div>
                <div class="menu-block customscroll">
                    <div class="sidebar-menu">
                        <ul id="accordion-menu">
                            <li>
                                <a href="../../home/" class="dropdown-toggle no-arrow">
                                    <span class="micon dw dw-house-1"></span><span class="mtext active">Accueil</span>
                                </a>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <span class="micon dw dw-user-11"></span><span class="mtext">Clients</span>
                                </a>
                                <ul class="submenu">
                                    <li><a href="../nos-clients/">Nos clients</a></li>
                                    <li><a href="../nos-prospects/">Nos Prospects</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <span class="micon dw dw-folder-28"></span><span class="mtext">Suivi</span>
                                </a>
                                <ul class="submenu">
                                    <li><a href="#">Suivi Comptable</a></li>
                                    <li><a href="#">Suivi Fiscal</a></li>
                                    <li><a href="#">Suivi Social</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle">
                                    <span class="micon dw dw-briefcase"></span><span class="mtext">Documents</span>
                                </a>
                                <ul class="submenu">
                                    <li><a href="#">Documents Juridiques</a></li>
                                    <li><a href="#">Documents Administratifs</a></li>
                                    <li><a href="#">Documents Internes</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="../collaborateurs/" class="dropdown-toggle no-arrow ">
                                    <span class="micon dw dw-user-2"></span><span class="mtext">Espace Collaborateurs</span>
                                </a>
                            </li>
                            <li>
                                <a href="../compte/" class="dropdown-toggle no-arrow ">
                                    <span class="micon dw dw-settings2"></span><span class="mtext">Mon compte</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="mobile-menu-overlay"></div>
            <!-- <nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm p-2"> -->


            <?php } ?>


            <div class="container">
                <a class="navbar-brand" href="../home/">
                    <?php /*
                    <?php if (!isset($_SESSION['auth'])) { ?>
                        <img src="../assets/images/logonotext.png" alt="" width="50" height="50" class="mr-3">
                    <?php } else { ?>
                        <img src="../assets/images/logonotextwhite.png" alt="" width="50" height="50" class="mr-3">
                    <?php } ?>
                        */ ?>


                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>


                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="../welcome">Welcome</a>
                        </li>

                        <?php if (!isset($_SESSION['auth'])) { ?>

                            <li class="nav-item">
                                <a class="nav-link" href="../contact">Contact Us</a>
                            </li>
                            
                            <li class="nav-item">
                                <a class="nav-link" href="../login">Login</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="../register">Signup</a>
                            </li>

                        <?php } else { ?>

                            <li class="nav-item">
                                <a class="nav-link" href="../dashboard">Dashboard</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="../home">Home</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="../contact">Contact Us</a>
                            </li>

                            <div class="dropdown">
                                <button class="btn btn-dark dropdown-toggle" type="button" id="imgdropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img class="navbar-img" src="../assets/uploads/users/<?php echo $_SESSION['profile_image'] ?>">
                                    <span class="caret"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="imgdropdown">
                                    <a class="dropdown-item text-muted" href="../profile"><i class="fa fa-user pr-2"></i> Profile</a>
                                    <a class="dropdown-item text-muted" href="../profile-edit"><i class="fa fa-pencil-alt pr-2"></i> Edit Profile</a>
                                    <a class="dropdown-item text-muted" href="../logout"><i class="fa fa-running pr-2"></i> Logout</a>
                                </div>
                            </div>

                        <?php } ?>





                    </ul>
                </div>
            </div>
            </nav>