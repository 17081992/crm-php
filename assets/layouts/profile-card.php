


<div class='card card-profile text-center box-shadow bg-white'>

    <?php if (isset($_SESSION['auth'])) { ?>
    <div class='card-block'>
        <a href='../../profile'>
            <img src='../assets/uploads/users/<?php echo $_SESSION['profile_image']; ?>' class='card-img-profile' alt="profile image">
        </a>
        <a href="../../profile-edit">
            <i class="fa fa-pencil-alt fa-1x edit-profile" aria-hidden="true"></i>
        </a>
        <div class="profile-info">
            <div class="small">
                <h5 class="text-center h5 mb-0">
                    <?php if ($_SESSION['gender'] == 'm'){ ?>
                        <i class="fa fa-male"></i>
                    <?php } elseif ($_SESSION['gender'] == 'f'){ ?>
                        <i class="fa fa-female"></i>
                    <?php } ?>
                    <?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?>
                </h5>
            </div>
            <div class="text-center text-muted font-14">
                <?php echo $_SESSION['email']; ?>
                <br/>
                <?php echo $_SESSION['role']; ?>
            </div>

        </div>
    </div>
    
    <?php }  ?>

</div>