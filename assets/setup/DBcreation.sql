
--
-- Database: `klik_loginsystem`
--

create schema klik_loginsystem;
use klik_loginsystem;

-- --------------------------------------------------------

--
-- Table structure for table `auth_tokens`
--

CREATE TABLE `auth_tokens` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `auth_type` varchar(255) NOT NULL,
  `selector` text NOT NULL,
  `token` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `expires_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `profile_image` varchar(255) NOT NULL DEFAULT '_defaultUser.png',
  `verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--
-- auto-generated definition
CREATE TABLE `client` (
    `id` int auto_increment primary key,
    `raison-social` varchar(100) not null,
    `nom-commercial` varchar(100) not null,
    `forme-juridique` varchar(100) not null,
    `code-naf` varchar(100) not null,
    `objet-social` text not null,
    `capital-social` varchar(100) not null,
    `dont-libere` varchar(100) not null,
    `nb-siren` int(9) not null,
    `date-immatriculation` date not null,
    `s-ligne1` text not null,
    `s-ligne2` text not null,
    `s-code-postal` varchar(50) not null,
    `s-ville` varchar(50) not null,
    president tinyint(1) not null,
    `directeur-general` tinyint(1) not null,
    `nom` varchar(255) not null,
    `prenom` varchar(255) not null,
    `civilite` varchar(255) not null,
    `etat-civile` varchar(255) not null,
    `mail` varchar(255) not null,
    `telephone` varchar(255) not null,
    `nationalite` varchar(50)  not null,
    `date-naissance` date not null,
    `ville-naissance` varchar(50)  not null,
    `pays-naissance` varchar(50)  not null,
    `adresse` tinyint(1) not null,
    `adr-ligne1` text not null,
    `adr-ligne2` text not null,
    `adr-code-postale` varchar(50)  not null,
    `adr-ville` varchar(50) not null,
    `actionnaire` tinyint(1)   not null,
    `pourcentage` varchar(11)  not null,
    `id-actionnaire` int not null,
    `mail-gouv` varchar(50)  not null,
    `password-gouv` varchar(50)  not null,
    `active` tinyint(1) not null,
    `regime-impot` varchar(255) not null,
    `tva` varchar(255) not null,
    `date-cloture-premier-exercice` date not null,
    `date-cloture` date not null,
    `adhesion` tinyint(1) not null,
    `adhesion-faite` tinyint(1) not null,
    `nb-adherant` varchar(50) not null,
    `cga` varchar(255) not null,
    `bulletin-adhesion` varchar(255) not null,
    `expert` varchar(50)  not null,
    `collab` varchar(50)  not null,
    `tarif-compta` varchar(11)  not null,
    `tarif-bilan` varchar(11)  not null,
    `reglement` text not null,
    constraint actclt
        foreign key (`id-actionnaire`) references actionnaire (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



-- --------------------------------------------------------

--
-- Table structure for table `prospect`
--

CREATE TABLE `prospect` (
    `id` int auto_increment primary key,
    `raison-social`varchar(100) not null,
    `nom-commercial`varchar(100) not null,
    `forme-juridique`varchar(100) not null,
    `code-naf` varchar(100) not null,
    `objet-social` text not null,
    `capital-social` varchar(100) not null,
    `dont-libere` varchar(100) not null,
    `siren-en-cours` tinyint(1)   not null,
    `date-signature-statuts` date not null,
    `s-ligne1` text not null,
    `s-ligne2` text not null,
    `s-code-postal` varchar(50) not null,
    `s-ville` varchar(50) not null,
    `president` tinyint(1) not null,
    `directeur-general` tinyint(1) not null,
    `nom` varchar(255) not null,
    `prenom` varchar(255) not null,
    `civilite` varchar(255) not null,
    `etat-civile` varchar(255) not null,
    `mail` varchar(255) not null,
    `telephone` varchar(255) not null,
    `nationalite` varchar(50)  not null,
    `date-naissance` date not null,
    `ville-naissance` varchar(50) not null,
    `pays-naissance` varchar(50) not null,
    `adresse` tinyint(1) not null,
    `adr-ligne1` text not null,
    `adr-ligne2` text not null,
    `adr-code-postale` varchar(50) not null,
    `adr-ville` varchar(50)  not null,
    `nom-pere` varchar(50)  not null,
    `prenom-pere` varchar(50)  not null,
    `nom-mere` varchar(50) not null,
    `prenom-mere` varchar(50) not null,
    `banque` tinyint(1) not null,
    `mention-speciale` text not null,
    `actionnaire` tinyint(1)   not null,
    `pourcentage` varchar(11)  not null,
    `id-actionnaire` int not null,
    `mail-gouv` varchar(50)  not null,
    `password-gouv` varchar(50)  not null,
    `active` tinyint(1)   not null,
    `regime-impot` varchar(255) not null,
    `tva` varchar(255) not null,
    `date-cloture-premier-exercice` date not null,
    `date-cloture` date not null,
    `adhesion` tinyint(1) not null,
    `adhesion-faite` tinyint(1)   not null,
    `nb-adherant` varchar(50)  not null,
    `cga` varchar(255) not null,
    `bulletin-adhesion` varchar(255) not null,
    `expert` varchar(50)  not null,
    `collab` varchar(50)  not null,
    `tarif-compta` varchar(11)  not null,
    `tarif-bilan` varchar(11)  not null,
    `reglement` text not null,
    constraint actpros
        foreign key (`id-actionnaire`) references actionnaire (id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


-- --------------------------------------------------------

--
-- Table structure for table `actionnaire`
--
create table `actionnaire` (
    `id` int not null primary key,
    `a-president` tinyint(1) not null,
    `a-directeur-general` tinyint(1) not null,
    `a-nom` varchar(50) not null,
    `a-prenom` varchar(50) not null,
    `a-civilite` varchar(50)  not null,
    `a-etat-civile` varchar(255) not null,
    `a-mail` varchar(50) not null,
    `a-telephone` int(50) not null,
    `a-nationalite` varchar(50) not null,
    `a-date-naissance` date not null,
    `a-pays-naissance` varchar(50) not null,
    `a-adresse` tinyint(1) not null,
    `a-adr-ligne1` text not null,
    `a-adr-ligne2` text not null,
    `a-adr-code-postale` varchar(50) not null,
    `a-adr-ville` varchar(50)  not null,
    `a-pourcentage` varchar(50)  not null
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for table `auth_tokens`
--

ALTER TABLE `auth_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `users`
--

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `id` (`id`,`email`);

--
-- AUTO_INCREMENT for table `auth_tokens`
--
ALTER TABLE `auth_tokens`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
COMMIT;
