<?php

define('TITLE', "Espace collaborateurs");
include '../assets/layouts/header.php';
//check_verified();

?>

    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="title">
                        <h4>Espace Collaborateurs</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="index.html">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Espace Collaborateurs</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
                <div class="pd-20 card-box height-100-p">
                    <div class="profile-photo">
                        <a href="modal" data-toggle="modal" data-target="#modal" class="edit-avatar"><i class="fa fa-pencil"></i></a>
                        <img src="../assets/uploads/users/<?php echo $_SESSION['profile_image']; ?>" alt="..." width="130" class="mb-2 img-thumbnail">
                        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-body pd-5">
                                        <div class="img-container">
                                            <img id="image" src="../vendors/images/photo2.jpg" alt="Picture">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="submit" value="Valider" class="btn btn-primary">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-info">
                        <p class="small"><h5 class="text-center h5 mb-0">

                            <?php if ($_SESSION['gender'] == 'm'){ ?>

                                <i class="fa fa-male"></i>


                            <?php } elseif ($_SESSION['gender'] == 'f'){ ?>

                                <i class="fa fa-female"></i>

                            <?php } ?>

                            <?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?>
                        </h5>
                        </p>

                        <p class="text-center text-muted font-14">
                            <?php echo $_SESSION['email']; ?>
                            <br/>
                            <?php echo $_SESSION['role']; ?>
                        </p>

                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
                <div class="card-box height-100-p overflow-hidden">
                    <div class="profile-tab height-100-p">
                        <div class="tab height-100-p">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#setting" role="tab">Paramètres du compte</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#timeline" role="tab">Cartes</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#tasks" role="tab">Tâches</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!-- Setting Tab start -->
                                <div class="tab-pane fade height-100-p" id="setting" role="tabpanel">
                                    <div class="profile-setting">
                                        <form>
                                            <ul class="profile-edit-list row">
                                                <li class="col-md-6">
                                                    <h4 class="text-blue h5 mb-20">Modifier vos informations personnelles</h4>
                                                    <div class="form-group">
                                                        <label>Civilité</label>
                                                        <div class="d-flex">
                                                            <div class="custom-control custom-radio mb-5 mr-20">
                                                                <input type="radio" id="male" name="customRadio" class="custom-control-input" checked>
                                                                <label class="custom-control-label weight-400" for="male">Homme</label>
                                                            </div>
                                                            <div class="custom-control custom-radio mb-5">
                                                                <input type="radio" id="female" name="customRadio" class="custom-control-input">
                                                                <label class="custom-control-label weight-400" for="female">Femme</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ec2">Rôle <sup class="text-danger">*</sup></label>
                                                        <select id="ec2" class="custom-select form-control">
                                                            <option selected="">Choisir...</option>
                                                            <option>Expert comptable</option>
                                                            <option>Collaborateur</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Nom</label>
                                                        <input class="form-control form-control-lg" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Prénom</label>
                                                        <input class="form-control form-control-lg" type="text">
                                                    </div>


                                                </li>
                                                <li class="col-md-6">
                                                    <h4 class="text-blue h5 mb-20">Modifier vos accées</h4>
                                                    <div class="form-group">
                                                        <label for="email">Email</label>
                                                        <input id="email" class="form-control form-control-lg" type="email">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="password-field">Mot de passe</label>
                                                        <input id="password-field" type="password" class="form-control" name="password" value="ADEXCA75012">
                                                        <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                    </div>
                                                    <div class="form-group">
                                                        <label  class="weight-600">Voulez-vous changer votre mot de passe ? <sup class="text-danger">*</sup></label>
                                                        <div class="custom-control custom-radio mb-5">
                                                            <input type="radio" id="oui6" name="ouinon6" class="custom-control-input">
                                                            <label for="oui6" class="custom-control-label">Oui</label>
                                                        </div>
                                                        <div class="custom-control custom-radio mb-5">
                                                            <input type="radio" id="non6" name="ouinon6" class="custom-control-input" checked>
                                                            <label for="non6" class="custom-control-label">Non</label>
                                                        </div>
                                                    </div>
                                                    <div id="divshow7">
                                                        <h6 class="mb-20">Saisir un nouveau mot de passe</h6>
                                                        <div class="input-group custom">
                                                            <input type="text" class="form-control form-control-lg" placeholder="Tapez un nouveau mot de passe">
                                                            <div class="input-group-append custom">
                                                                <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                                                            </div>
                                                        </div>
                                                        <div class="input-group custom">
                                                            <input type="text" class="form-control form-control-lg" placeholder="Retapez le nouveau mot de passe">
                                                            <div class="input-group-append custom">
                                                                <span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-0">
                                                        <input type="submit" class="btn btn-primary float-right" value="Valider les modifications">
                                                    </div>
                                                </li>
                                            </ul>
                                        </form>
                                    </div>
                                </div>
                                <!-- Setting Tab End -->
                                <!-- Timeline Tab start -->
                                <div class="tab-pane fade show active" id="timeline" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="profile-timeline">
                                            <div class="timeline-month">
                                                <h5>Décembre, 2020</h5>
                                            </div>
                                            <div class="profile-timeline-list">
                                                <ul>
                                                    <li>
                                                        <div class="date">21 Dec</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte ajoutée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">09:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">17 Dec</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte terminée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">10:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">11 Dec</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte en attente</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">16:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">03 Dec</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte ajoutée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">11:30</div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="timeline-month">
                                                <h5>Novembre, 2020</h5>
                                            </div>
                                            <div class="profile-timeline-list">
                                                <ul>
                                                    <li>
                                                        <div class="date">29 Nov</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte ajoutée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">09:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">18 Nov</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte terminée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">10:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">08 Nov</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte en attente</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">16:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">09 Nov</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte ajoutée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">11:30</div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="timeline-month">
                                                <h5>Octobre, 2020</h5>
                                            </div>
                                            <div class="profile-timeline-list">
                                                <ul>
                                                    <li>
                                                        <div class="date">21 Oct</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte ajoutée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">09:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">18 Oct</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte terminée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">10:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">17 Oct</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte en attente</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">16:30</div>
                                                    </li>
                                                    <li>
                                                        <div class="date">01 Oct</div>
                                                        <div class="task-name"><i class="ion-android-alarm-clock"></i> Carte ajoutée</div>
                                                        <p>descriptif du contenu de la carte assignée</p>
                                                        <div class="task-time">11:30</div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Timeline Tab End -->
                                <!-- Tasks Tab start -->
                                <div class="tab-pane fade" id="tasks" role="tabpanel">
                                    <div class="pd-20 profile-task-wrap">
                                        <div class="container pd-0">
                                            <!-- Open Task start -->
                                            <div class="task-title row align-items-center">
                                                <div class="col-md-8 col-sm-12">
                                                    <h5>Tâches en attente</h5>
                                                </div>
                                                <div class="col-md-4 col-sm-12 text-right">
                                                    <a href="task-add" data-toggle="modal" data-target="#task-add" class="bg-light-blue btn text-blue weight-500"><i class="ion-plus-round"></i> Ajouter</a>
                                                </div>
                                            </div>
                                            <div class="profile-task-list pb-30">
                                                <ul>
                                                    <li>
                                                        <div class="custom-control custom-checkbox mb-5">
                                                            <input type="checkbox" class="custom-control-input" id="task-1">
                                                            <label class="custom-control-label" for="task-1"></label>
                                                        </div>
                                                        <div class="task-type">Email</div>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id ea earum.
                                                        <div class="task-assign">Assigné à Emmanuella <div class="due-date">Le <span>22 Octobre 2020</span></div></div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox mb-5">
                                                            <input type="checkbox" class="custom-control-input" id="task-2">
                                                            <label class="custom-control-label" for="task-2"></label>
                                                        </div>
                                                        <div class="task-type">Email</div>
                                                        Lorem ipsum dolor sit amet.
                                                        <div class="task-assign">Assigné à Emmanuella <div class="due-date">Le <span>22 Octobre 2020</span></div></div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox mb-5">
                                                            <input type="checkbox" class="custom-control-input" id="task-3">
                                                            <label class="custom-control-label" for="task-3"></label>
                                                        </div>
                                                        <div class="task-type">Email</div>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                        <div class="task-assign">Assigné à Emmanuella <div class="due-date">Le <span>22 Octobre 2020</span></div></div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox mb-5">
                                                            <input type="checkbox" class="custom-control-input" id="task-4">
                                                            <label class="custom-control-label" for="task-4"></label>
                                                        </div>
                                                        <div class="task-type">Email</div>
                                                        Lorem ipsum dolor sit amet. Id ea earum.
                                                        <div class="task-assign">Assigné à Emmanuella <div class="due-date">Le <span>22 Octobre 2020</span></div></div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- Open Task End -->
                                            <!-- Close Task start -->
                                            <div class="task-title row align-items-center">
                                                <div class="col-md-12 col-sm-12">
                                                    <h5>Tâches finis</h5>
                                                </div>
                                            </div>
                                            <div class="profile-task-list close-tasks">
                                                <ul>
                                                    <li>
                                                        <div class="custom-control custom-checkbox mb-5">
                                                            <input type="checkbox" class="custom-control-input" id="task-close-1" checked="" disabled="">
                                                            <label class="custom-control-label" for="task-close-1"></label>
                                                        </div>
                                                        <div class="task-type">Email</div>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id ea earum.
                                                        <div class="task-assign">Assigné à Emmanuella <div class="due-date">Le <span>22 Octobre 2020</span></div></div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox mb-5">
                                                            <input type="checkbox" class="custom-control-input" id="task-close-2" checked="" disabled="">
                                                            <label class="custom-control-label" for="task-close-2"></label>
                                                        </div>
                                                        <div class="task-type">Email</div>
                                                        Lorem ipsum dolor sit amet.
                                                        <div class="task-assign">Assigné à Emmanuella <div class="due-date">Le <span>22 Octobre 2020</span></div></div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox mb-5">
                                                            <input type="checkbox" class="custom-control-input" id="task-close-3" checked="" disabled="">
                                                            <label class="custom-control-label" for="task-close-3"></label>
                                                        </div>
                                                        <div class="task-type">Email</div>
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                                                        <div class="task-assign">Assigné à Emmanuella <div class="due-date">Le <span>22 Octobre 2020</span></div></div>
                                                    </li>
                                                    <li>
                                                        <div class="custom-control custom-checkbox mb-5">
                                                            <input type="checkbox" class="custom-control-input" id="task-close-4" checked="" disabled="">
                                                            <label class="custom-control-label" for="task-close-4"></label>
                                                        </div>
                                                        <div class="task-type">Email</div>
                                                        Lorem ipsum dolor sit amet. Id ea earum.
                                                        <div class="task-assign">Assigné à Emmanuella <div class="due-date">Le <span>22 Octobre 2020</span></div></div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <!-- Close Task start -->
                                            <!-- add task popup start -->
                                            <div class="modal fade customscroll" id="task-add" tabindex="-1" role="dialog">
                                                <div class="modal-dialog modal-dialog-centered" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLongTitle">Ajouter des tâches</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Close Modal">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body pd-0">
                                                            <div class="task-list-form">
                                                                <ul>
                                                                    <li>
                                                                        <form>
                                                                            <div class="form-group row">
                                                                                <label class="col-md-4">Type</label>
                                                                                <div class="col-md-8">
                                                                                    <input type="text" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-md-4">Message</label>
                                                                                <div class="col-md-8">
                                                                                    <textarea class="form-control"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-md-4">Assignée à</label>
                                                                                <div class="col-md-8">
                                                                                    <select class="selectpicker form-control" data-style="btn-outline-primary" title="Choisir..." multiple="" data-selected-text-format="count" data-count-selected-text= "{0} people selected">
                                                                                        <option>Aryé</option>
                                                                                        <option>Dylan</option>
                                                                                        <option>Emanuella</option>
                                                                                        <option>Maudline</option>
                                                                                        <option>Dan</option>
                                                                                        <option>Chinh</option>
                                                                                        <option>Betsalel</option>
                                                                                        <option>Mikael</option>
                                                                                        <option>Sami</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row mb-0">
                                                                                <label class="col-md-4">Date</label>
                                                                                <div class="col-md-8">
                                                                                    <input type="text" class="form-control date-picker">
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </li>
                                                                    <li>
                                                                        <a href="javascript:;" class="remove-task"  data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Remove Task"><i class="ion-minus-circled"></i></a>
                                                                        <form>
                                                                            <div class="form-group row">
                                                                                <label class="col-md-4">Type</label>
                                                                                <div class="col-md-8">
                                                                                    <input type="text" class="form-control">
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-md-4">Message</label>
                                                                                <div class="col-md-8">
                                                                                    <textarea class="form-control"></textarea>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row">
                                                                                <label class="col-md-4">Assignée à</label>
                                                                                <div class="col-md-8">
                                                                                    <select class="selectpicker form-control" data-style="btn-outline-primary" title="Choisir..." multiple="" data-selected-text-format="count" data-count-selected-text= "{0} people selected">
                                                                                        <option>Aryé</option>
                                                                                        <option>Dylan</option>
                                                                                        <option>Emanuella</option>
                                                                                        <option>Maudline</option>
                                                                                        <option>Dan</option>
                                                                                        <option>Chinh</option>
                                                                                        <option>Betsalel</option>
                                                                                        <option>Mikael</option>
                                                                                        <option>Sami</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group row mb-0">
                                                                                <label class="col-md-4">Date</label>
                                                                                <div class="col-md-8">
                                                                                    <input type="text" class="form-control date-picker">
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="add-more-task">
                                                                <a href="#" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Add Task"><i class="ion-plus-circled"></i> Ajouter une autre tâche</a>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-primary">Ajouter</button>
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- add task popup End -->
                                        </div>
                                    </div>
                                </div>
                                <!-- Tasks Tab End -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

include '../assets/layouts/footer.php'

?>