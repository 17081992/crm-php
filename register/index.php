<?php

define('TITLE', "Signup");
include '../assets/layouts/header.php';
check_logged_out();

?>


<div class="container">
    <div class="row">
        <div class="col-md-4">

        </div>
        <div class="col-lg-4">

            <form class="form-auth" action="includes/register.inc.php" method="post" enctype="multipart/form-data">

                <?php insert_csrf_token(); ?>

                <div class="picCard text-center">
                    <div class="avatar-upload">
                        <div class="avatar-preview text-center">
                            <div id="imagePreview" style="background-image: url( ../assets/uploads/users/_defaultUser.png );"></div>
                        </div>
                        <div class="avatar-edit">
                            <input name='avatar' id="avatar" class="fas fa-pencil" type='file' />
                            <label for="avatar"></label>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <sub class="text-danger">
                        <?php
                            if (isset($_SESSION['ERRORS']['imageerror']))
                                echo $_SESSION['ERRORS']['imageerror'];

                        ?>
                    </sub>
                </div>

                <h6 class="h3 mt-3 mb-3 font-weight-normal text-muted text-center">Créer un compte</h6>

                <div class="text-center mb-3">
                    <small class="text-success font-weight-bold">
                        <?php
                            if (isset($_SESSION['STATUS']['signupstatus']))
                                echo $_SESSION['STATUS']['signupstatus'];

                        ?>
                    </small>
                </div>

                <?php 
                    /*
                    <div class="form-group">
                        <label for="username" class="sr-only">Username</label>
                        <input type="text" id="username" name="username" class="form-control" placeholder="Username" required autofocus>
                        <sub class="text-danger">
                            <?php
                                if (isset($_SESSION['ERRORS']['usernameerror']))
                                    echo $_SESSION['ERRORS']['usernameerror'];

                            ?>
                        </sub>
                    </div>
                    */ 
                ?>

                <div class="form-group">
                    <label for="email" class="sr-only">Adresse mail</label>
                    <input type="email" id="email" name="email" class="form-control" placeholder="Adresse mail" required autofocus>
                    <sub class="text-danger">
                        <?php
                            if (isset($_SESSION['ERRORS']['emailerror']))
                                echo $_SESSION['ERRORS']['emailerror'];

                        ?>
                    </sub>
                </div>

                <div class="form-group mt-4">
                    <label for="role" class="sr-only">Rôle</label>
                    <select id="role" name="role" class="form-control custom-select" placeholder="role" required>
                        <option value="expert_comptable">Expert comptable</option>
                        <option value="collaborateur">collaborateur</option>
                    </select>
                </div>
                <?php /*
                <div class="form-group">
                    <input id="currentpassword" type="password" class="form-control" name="password" placeholder="mot de passe actuel" autocomplete="new-password">
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>

                </div>
                <div class="form-group">
                    <input id="newpassword" type="password" class="form-control" name="newpassword" placeholder="nouveau mot de passe" autocomplete="new-password">
                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                </div>
                */ ?>
                <div class="form-group">
                    <label for="password" class="sr-only">Mot de passe</label>
                    <input type="password" id="password" name="password" class="form-control" placeholder="Mot de passe" required>
                </div>
                
                <div class="form-group mb-4">
                    <label for="confirmpassword" class="sr-only">Confirmer le Mot de passe</label>
                    <input type="password" id="confirmpassword" name="confirmpassword" class="form-control" placeholder="Retapez le mot de passe" required>
                    <sub class="text-danger mb-4">
                        <?php
                            if (isset($_SESSION['ERRORS']['passworderror']))
                                echo $_SESSION['ERRORS']['passworderror'];

                        ?>
                    </sub>
                </div>

                <span class="h5 mb-3 font-weight-normal text-muted text-center">Optionnels</span>

                <br><br>

                <div class="form-group">
                    <label for="first_name" class="sr-only">Nom</label>
                    <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Nom">
                </div>

                <div class="form-group">
                    <label for="last_name" class="sr-only">Prénom</label>
                    <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Prénom">
                </div>

                <div class="form-group">
                    <label>Civilité</label>

                    <div class="custom-control custom-radio custom-control">
                        <input type="radio" id="male" name="gender" class="custom-control-input" value="m">
                        <label class="custom-control-label" for="male">Homme</label>
                    </div>
                    <div class="custom-control custom-radio custom-control">
                        <input type="radio" id="female" name="gender" class="custom-control-input" value="f">
                        <label class="custom-control-label" for="female">Femme</label>
                    </div>
                </div>

                <button class="btn btn-lg btn-primary btn-block" type="submit" name='signupsubmit'>Signup</button>

            

            </form>

        </div>
        <div class="col-md-4">

        </div>
    </div>
</div>



<?php

include '../assets/layouts/footer.php'

?>

<script type="text/javascript">
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);

            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar").change(function() {
        console.log("here");
        readURL(this);
    });

    $(".show_hide_password a").on('click', function(event) {

        event.preventDefault();

        if($(this).prev('input').attr("type") == "text"){

            $(this).prev('input').attr('type', 'password');
            $(this).find('i').addClass( "fa-eye-slash" );
            $(this).find('i').removeClass( "fa-eye" );

        }else if($(this).prev('input').attr("type") == "password"){

            $(this).prev('input').attr('type', 'text');
            $(this).find('i').removeClass( "fa-eye-slash" );
            $(this).find('i').addClass( "fa-eye" );

        }
    });
</script>