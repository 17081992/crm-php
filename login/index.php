<?php

define('TITLE', "Login");
include '../assets/layouts/header.php';
check_logged_out();
?>

    <div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-lg-7">
                    <img src="../assets/images/login-page-img.png" alt="">
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="login-box bg-white box-shadow border-radius-10">
                        <div class="login-title">
                            <h2 class="text-center text-primary">Se connecter au CRM Adexca</h2>
                        </div>
                        <form class="form-auth" action="includes/login.inc.php" method="post">
                            <?php insert_csrf_token(); ?>
                            <div class="text-center mb-3">
                                <small class="text-success font-weight-bold">
                                    <?php
                                    if (isset($_SESSION['STATUS']['loginstatus']))
                                        echo $_SESSION['STATUS']['loginstatus'];

                                    ?>
                                </small>
                            </div>

                            <div class="form-group">
                                <label for="email" class="sr-only">Email</label>
                                <input type="text" id="email" name="email" class="form-control" placeholder="email" required autofocus>
                                <sub class="text-danger">
                                    <?php
                                    if (isset($_SESSION['ERRORS']['nouser']))
                                        echo $_SESSION['ERRORS']['nouser'];
                                    ?>
                                </sub>
                            </div>

                            <div class="form-group show_hide_password">
                                <label for="password-field">Mot de passe</label>
                                <div class="d-flex">
                                    <input id="password-field" type="password" class="form-control" name="password">
                                    <a class="btn"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                </div>
                                <sub class="text-danger">
                                    <?php
                                    if (isset($_SESSION['ERRORS']['wrongpassword']))
                                        echo $_SESSION['ERRORS']['wrongpassword'];
                                    ?>
                                </sub>
                            </div>
                            
                            <div class="my-1 m../registerb-2 d-flex justify-content-between">
                                <div class="custom-control custom-checkbox mr-sm-2">
                                    <input type="checkbox" class="custom-control-input" id="rememberme" name="rememberme">
                                    <label class="custom-control-label" for="rememberme">Sauvegarder</label>
                                </div>
                                <p class="text-muted float-right"><a href="../reset-password/">Mot de passe oublié ?</a></p>
                            </div>

                            <button class="btn btn-lg btn-primary btn-block" type="submit" value="loginsubmit" name="loginsubmit">Se connecter</button>


                            <div class="row">
                                <div class="col-sm-12">
                                   <div class="font-16 weight-600 pt-10 pb-10 text-center" data-color="#707373">ou</div>
                                    <div class="input-group mb-0">
                                        <a class="btn btn-outline-primary btn-lg btn-block" href="../register">Créer un compte</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php

include '../assets/layouts/footer.php'

?>
<script>

    $(document).ready(function() {
        $(".show_hide_password a").on('click', function(event) {

            event.preventDefault();

            if($(this).prev('input').attr("type") == "text"){

                $(this).prev('input').attr('type', 'password');
                $(this).find('i').addClass( "fa-eye-slash" );
                $(this).find('i').removeClass( "fa-eye" );

            }else if($(this).prev('input').attr("type") == "password"){

                $(this).prev('input').attr('type', 'text');
                $(this).find('i').removeClass( "fa-eye-slash" );
                $(this).find('i').addClass( "fa-eye" );

            }
        });
    });

</script>
