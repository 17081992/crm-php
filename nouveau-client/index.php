<?php

define('TITLE', "nos Clients");
include '../assets/layouts/header.php';
//check_verified();

?>
<link rel="stylesheet" type="text/css" href="../src/plugins/jquery-steps/jquery.steps.css">
<link rel="stylesheet" type="text/css" href="../vendors/styles/countrySelect.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css">
<!-- switchery css -->
<link rel="stylesheet" type="text/css" href="../src/plugins/switchery/switchery.min.css">
<!-- bootstrap-tagsinput css -->
<link rel="stylesheet" type="text/css" href="../src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- bootstrap-touchspin css -->
<link rel="stylesheet" type="text/css" href="../src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.css">
<link rel="stylesheet" type="text/css" href="../vendors/styles/style.css">

    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Ajouter un nouveau client</h4>
                    </div>cd//..
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/">Accueil</a></li>
                            <li class="breadcrumb-item"><a href="../nos-clients/">Clients</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Ajouter un nouveau client</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>

        <div class="pd-20 card-box mb-30">
            <!-- <div class="clearfix">
                <h4 class="text-blue h4"> Informations de l'entreprise</h4>
                <p class="mb-30">Saisir les informations relatives a une entreprise</p>
            </div> -->
            <div class="wizard-content">
                <form class="tab-wizard2 wizard-circle wizard">
                    <h5>Entreprise</h5>
                    <section>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="rs">Raison sociale <sup class="text-danger">*</sup></label>
                                    <input id="rs" type="text" class="form-control" placeholder="Ex: Adexca">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nc">Nom commercial </label>
                                    <input id="nc" type="text" class="form-control" placeholder="Ex: Adexca">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fj">Forme juridique <sup class="text-danger">*</sup></label>
                                    <select id="fj" class="custom-select form-control">
                                        <option selected="">Choisir...</option>
                                        <option>SASU - Société par actions simplifiée unipersonnelle</option>
                                        <option>SAS - Société par actions simplifiée</option>
                                        <option>SA - Société anonyme</option>
                                        <option>SARL - Société à responsabilité limitée</option>
                                        <option>EURL - Entreprise unipersonnelle à responsabilité limitée</option>
                                        <option>EIRL - Entreprise individuelle à responsabilité limitée</option>
                                        <option>EI - Entreprise individuelle</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cn">Code NAF  <sup class="text-danger">*</sup></label>
                                    <input id="cn" type="text" class="form-control" placeholder="Ex: 6920Z">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="os">Objet social <sup class="text-danger">*</sup></label>
                                    <textarea id="os" class="form-control" rows="2" placeholder="Ex: Activités comptables"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="cs">Capital social <sup class="text-danger">*</sup></label>
                                    <input id="cs" type="text" class="form-control" placeholder="Ex: 1 000,00 €">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dl">dont libéré <sup class="text-danger">*</sup></label>
                                    <input id="dl" type="text" class="form-control" placeholder="Ex: 1 000,00 €">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ns">Numéro SIREN <sup class="text-danger">*</sup></label>
                                    <input id="ns" class="form-control form-control-inline input-medium numeric"
                                           size="16" type="text" value="" maxlength="9"
                                           onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57"
                                           placeholder="Ex: 878057249">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="di">Date d’immatriculation <sup class="text-danger">*</sup></label>
                                    <input id="di" type="text" class="form-control date-picker" placeholder="Sélectionner une date">
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <h5 class="my-3">Siège social :</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="al1">Ligne 1 <sup class="text-danger">*</sup></label>
                                    <input id="al1" type="text" class="form-control" required="" placeholder="Ex: 12 rue de Montera">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="al2">Ligne 2 <span class="text-muted">(Optionnel)</span></label>
                                <input id="al2" type="text" class="form-control" placeholder="Ex: RDC étage 1">
                            </div>
                        </div>
                        <div class="row mb-20">
                            <div class="col-md-6">
                                <label for="cp3">Code postal <sup class="text-danger">*</sup></label>
                                <input id="cp3" type="text" class="form-control numeric"
                                       placeholder="Ex: 75012"
                                       maxlength="5"
                                       onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
                            </div>
                            <div class="col-md-6">
                                <label for="vl">Ville <sup class="text-danger">*</sup></label>
                                <input id="vl" type="text" class="form-control" placeholder="Ex: Paris">
                            </div>
                        </div>
                    </section>
                    <!-- Step 2 -->
                    <h5>Représentant légal</h5>
                    <section>
                        <div class="row justify-content-center my-4">
                            <div class="form-group px-5">
                                <label><input type="checkbox" checked class="switch-btn" data-size="small" data-color="#0099ff"> President</label>
                            </div>
                            <div class="form-group px-5">
                                <label><input type="checkbox" checked class="switch-btn" data-size="small" data-color="#0099ff"> Directeur Général</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Nom <sup class="text-danger">*</sup></label>
                                    <input id="name" type="text" class="form-control" placeholder="Ex: François">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="surname">Prénom <sup class="text-danger">*</sup></label>
                                    <input id="surname" type="text" class="form-control" placeholder="Ex: Dupont">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="sexe">Civilité <sup class="text-danger">*</sup></label>
                                    <select id="sexe" class="custom-select form-control">
                                        <option selected="">Choisir...</option>
                                        <option value="Amsterdam">Homme</option>
                                        <option value="Berlin">Femme</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ec">Etat civile <sup class="text-danger">*</sup></label>
                                    <select id="ec" class="form-control">
                                        <option selected="">Choisir...</option>
                                        <option>Célibataire</option>
                                        <option>Pacsé(e)</option>
                                        <option>Marié(e) sous le régime de la communauté universelle</option>
                                        <option>Marié(e) sous le régime de la séparation de biens</option>
                                        <option>Divorcé(e)</option>
                                        <option>Veuf(ve)</option>
                                        <option>Autre</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mail">Adresse Email <sup class="text-danger">*</sup></label>
                                    <input id="mail" type="email" class="form-control" PLACEHOLDER="Ex: adconseils@outlook.com">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="w-100" for="phone0">Numéro de téléphone <sup class="text-danger">*</sup></label>
                                    <input id="phone0" type="text" class="form-control" name="phone" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                                    <span id="valid-msg0" class="hide text-success">✓ Valide</span>
                                    <span id="error-msg0" class="hide text-danger"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="w-100" for="nat">Nationalité <sup class="text-danger">*</sup></label>
                                    <input id="nat" type="text" class=" form-control countrypicker w-100" data-flag="true" />
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="dn">Date de naissance <sup class="text-danger">*</sup></label>
                                    <input id="dn" type="text" class="form-control date-picker" placeholder="Sélectionner une date">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label for="vn">Ville de naissance</label>
                                    <input id="vn" type="text" class="form-control" placeholder="Ex: Paris">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="w-100" for="pn">Pays de naissance <sup class="text-danger">*</sup></label>
                                    <input id="pn" type="text" class=" form-control countrypicker w-100" data-flag="true" />
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-12 my-3">
                                <label class="weight-600">L’adresse de domicile et de l'entreprise sont-elles les mêmes ? <sup class="text-danger">*</sup></label>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="oui1" name="customRadioon" class="custom-control-input">
                                    <label class="custom-control-label" for="oui1">Oui</label>
                                </div>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="non1" name="customRadioon" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="non1">non</label>
                                </div>
                            </div>
                        </div>
                        <div id="divshow">
                            <h5 class="my-3">Adresse de Domiciliation :</h5>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="al12">Ligne 1 <sup class="text-danger">*</sup></label>
                                        <input id="al12" type="text" class="form-control" required="" placeholder="Ex: 12 rue de Montera">
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="al22">Ligne 2 <span class="text-muted">(Optionnel)</span></label>
                                    <input id="al22" type="text" class="form-control" placeholder="Ex: RDC étage 1">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="cp1">Code postal <sup class="text-danger">*</sup></label>
                                    <input id="cp1" type="text" class="form-control numeric"
                                           placeholder="Ex: 75012"
                                           maxlength="5"
                                           onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
                                </div>
                                <div class="col-md-6">
                                    <label for="vl2">Ville <sup class="text-danger">*</sup></label>
                                    <input id="vl2" type="text" class="form-control" placeholder="Ex: Paris">
                                </div>
                            </div>
                        </div>

                        <!-- <h5 class="my-3">Filiation</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="np">Nom du père <sup class="text-danger">*</sup></label>
                                    <input id="np" type="text" class="form-control" required="" placeholder="Ex: François">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="pp">Prénom du père <sup class="text-danger">*</sup></label>
                                <input id="pp" type="text" class="form-control" placeholder="Dupont">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="njf">Nom de jeune fille de la mère  <sup class="text-danger">*</sup></label>
                                <input id="njf" type="text" class="form-control"required="" placeholder="Dupont">
                            </div>
                            <div class="col-md-6">
                                <label for="pm">Prénom de la mère <sup class="text-danger">*</sup></label>
                                <input id="pm" type="text" class="form-control" placeholder="Ex: Marie">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-12 my-3">
                                <label class="weight-600">Banque <sup class="text-danger">*</sup></label>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="qonto" name="customRadio_b" class="custom-control-input " checked>
                                    <label class="custom-control-label" for="qonto">Qonto</label>
                                </div>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="autre" name="customRadio_b" class="custom-control-input">
                                    <label class="custom-control-label" for="autre">Autre</label>
                                </div>
                            </div>
                        </div>
                        <div id="mention" class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="ms">Mention spéciale <sup class="text-danger">*</sup></label>
                                    <textarea id="ms" class="form-control" rows="2" placeholder="Ex : Article 6"></textarea>
                                </div>
                            </div>
                            <div class="pd-20 card-box mb-30" data-bgcolor="#ff2b2bb3" data-color="#ffff">
                                <ul>
                                    <li class="d-flex pb-20"><i class="dw dw-edit-file font-20 mr-2"></i>Article 33</li>
                                    <li>Tous pouvoirs sont conférés au porteur d'un original des présentes à l'effet d'accomplir les
                                        formalités de publicité, de dépôt et autres nécessaires pour parvenir à l'immatriculation de la Société
                                        au Registre du Commerce et des Sociétés.
                                        Dépôt du capital social auprès d'une étude notariale
                                        Ouverture d'un compte de transit à leurs noms auprès de Olinda SAS (QONTO), établissement
                                        de paiement agréé auprès de l'ACPR
                                        Ouverture d’un compte de paiement au nom de la Société auprès de OLINDA SAS (Qonto),
                                        établissement de paiement agréé auprès de l’ACPR. </li>
                                </ul>
                            </div>
                        </div>
                        -->
                        <hr/>
                        <div class="row">
                            <div class="col-12 my-3">
                                <label class="weight-600">Est t-il le seul actionnaire ? <sup class="text-danger">*</sup></label>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="seulact" name="customRadio_a" class="custom-control-input" checked>
                                    <label class="custom-control-label" for="seulact">Oui</label>
                                </div>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="autreact" name="customRadio_a" class="custom-control-input">
                                    <label class="custom-control-label" for="autreact">Non</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-4 col-lg-6 col-12 mb-20">
                                <label for="pc">Pourcentage actionnariat <sup class="text-danger">*</sup></label>
                                <input type="text" class="form-control" id="pc" placeholder="Ex: 100%">
                            </div>
                        </div>

                        <div  id="nvact" class="card mb-20">
                            <div class="card-body">
                                <h5 class="card-title">Ajouter un autre actionnaire</h5>
                                <p class="card-text">ici on peut mettre du texte explicatif ou une note</p>
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#staticBackdrop">
                                    Ajouter
                                </button>
                            </div>
                        </div>

                        <!-- /////////////////////////////  ajouter un nv actionnaire   //////////////////////////  -->
                        <!-- Modal -->
                        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                            <div class="modal-dialog modal-xl">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">Ajout d'un autre actionnaire</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body px-4">
                                        <div class="row justify-content-center my-4">
                                            <div class="form-group px-5">
                                                <label><input type="checkbox" checked class="switch-btn" data-size="small" data-color="#0099ff"> President</label>
                                            </div>
                                            <div class="form-group px-5">
                                                <label><input type="checkbox" checked class="switch-btn" data-size="small" data-color="#0099ff"> Directeur Général</label>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="name1">Nom <sup class="text-danger">*</sup></label>
                                                    <input id="name1" type="text" class="form-control" placeholder="Ex: François">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="surname1">Prénom <sup class="text-danger">*</sup></label>
                                                    <input id="surname1" type="text" class="form-control" placeholder="Ex: Dupont">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="sexe1">Civilité <sup class="text-danger">*</sup></label>
                                                    <select id="sexe1" class="custom-select form-control">
                                                        <option selected="">Choisir...</option>
                                                        <option value="Amsterdam">Homme</option>
                                                        <option value="Berlin">Femme</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="ec1">Etat civile <sup class="text-danger">*</sup></label>
                                                    <select id="ec1" class="form-control">
                                                        <option selected="">Choisir...</option>
                                                        <option>Célibataire</option>
                                                        <option>Pacsé(e)</option>
                                                        <option>Marié(e) sous le régime de la communauté universelle</option>
                                                        <option>Marié(e) sous le régime de la séparation de biens</option>
                                                        <option>Divorcé(e)</option>
                                                        <option>Veuf(ve)</option>
                                                        <option>Autre</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="mail1">Adresse Email <sup class="text-danger">*</sup></label>
                                                    <input id="mail1" type="email" class="form-control" PLACEHOLDER="Ex: adconseils@outlook.com">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="w-100" for="phone1">Numéro de téléphone <sup class="text-danger">*</sup></label>
                                                    <input id="phone1" type="text" class="form-control" name="phone" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" required>
                                                    <span id="valid-msg1" class="hide text-success">✓ Valide</span>
                                                    <span id="error-msg1" class="hide text-danger"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="w-100" for="nat1">Nationalité <sup class="text-danger">*</sup></label>
                                                    <input id="nat1" type="text" class=" form-control countrypicker w-100" data-flag="true" />
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="dn1">Date de naissance <sup class="text-danger">*</sup></label>
                                                    <input id="dn1" type="text" class="form-control date-picker" placeholder="Sélectionner une date">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="vn1">Ville de naissance <sup class="text-danger">*</sup></label>
                                                    <input id="vn1" type="text" class="form-control" placeholder="Ex: Paris">
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="w-100" for="pn1">Pays de naissance <sup class="text-danger">*</sup></label>
                                                    <input id="pn1" type="text" class=" form-control countrypicker w-100" data-flag="true" />
                                                </div>
                                            </div>
                                        </div>
                                        <hr/>
                                        <div class="row">
                                            <div class="col-12 my-3">
                                                <label class="weight-600">L’adresse de domicile et de l'entreprise sont-elles les mêmes ? <sup class="text-danger">*</sup></label>
                                                <div class="custom-control custom-radio mb-5">
                                                    <input type="radio" id="oui2" name="customRadioon2" class="custom-control-input">
                                                    <label class="custom-control-label" for="oui2">Oui</label>
                                                </div>
                                                <div class="custom-control custom-radio mb-5">
                                                    <input type="radio" id="non2" name="customRadioon2" class="custom-control-input" checked>
                                                    <label class="custom-control-label" for="non2">non</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="divshow2">
                                            <h5 class="my-3">Adresse de Domiciliation :</h5>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label for="al13">Ligne 1 <sup class="text-danger">*</sup></label>
                                                        <input id="al13" type="text" class="form-control" required="" placeholder="Ex: 12 rue de Montera">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <label for="al23">Ligne 2 <span class="text-muted">(Optionnel)</span></label>
                                                    <input id="al23" type="text" class="form-control" placeholder="Ex: RDC étage 1">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label for="cp2">Code postal <sup class="text-danger">*</sup></label>
                                                    <input id="cp2" type="text" class="form-control numeric"
                                                           placeholder="Ex: 75012"
                                                           maxlength="5"
                                                           onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57">
                                                </div>
                                                <div class="col-md-6">
                                                    <label for="vl3">Ville <sup class="text-danger">*</sup></label>
                                                    <input id="vl3" type="text" class="form-control" placeholder="Ex: Paris">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xl-4 col-lg-6 col-12 mt-15">
                                                <label for="pc2">Pourcentage actionnariat <sup class="text-danger">*</sup></label>
                                                <input type="text" class="form-control" id="pc2" placeholder="Ex: 50%">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Annuler</button>
                                        <button type="button" class="btn btn-primary">Enregistrer</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </section>
                    <!-- Step 3 -->
                    <h5>Option fiscale</h5>
                    <section>
                        <h5>Accès compte fiscal :</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="emailgouv">Email</label>
                                    <input id="emailgouv" class="form-control form-control-lg" value="" type="email" placeholder="Ex: adexca@exemple.fr">
                                </div>
                                <div class="form-group">
                                    <label for="dccf">Date de création de compte fiscal </label>
                                    <input id="dccf" type="text" class="form-control date-picker" placeholder="Sélectionner une date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="password-field">Mot de passe impôt.gouv</label>
                                    <input id="password-field" type="password" class="form-control" name="password" value="ADEXCA75012">
                                    <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                </div>
                                <div class="col-12 d-inline-flex mt-4 pt-3">
                                    <label class="weight-600">Activé ? <sup class="text-danger">*</sup></label>
                                    <div class="custom-control custom-radio pr-2">
                                        <input type="radio" id="oui3" name="customRadioon3" class="custom-control-input" checked>
                                        <label class="custom-control-label" for="oui3">Oui</label>
                                    </div>
                                    <div class="custom-control custom-radio pr-2">
                                        <input type="radio" id="non3" name="customRadioon3" class="custom-control-input">
                                        <label class="custom-control-label" for="non3">non</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row mt-4">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Régime d’imposition</label>
                                    <select class="custom-select2 form-control" name="state" style="width: 100%; height: 38px;">
                                        <option value="AK" selected="">Choisir...</option>
                                        <optgroup label="IR - Impôt sur le Revenu">
                                            <option value="AK">BIC - Bénéfices industriels et commerciaux</option>
                                            <option value="HI">BNC - Bénéfices non commerciaux</option>
                                            <option value="HI">RF - Revenu Foncier</option>
                                        </optgroup>
                                        <optgroup label="IS - Impôt sur les Sociétés">
                                            <option value="CA">IS - Impôt sur les Sociétés</option>
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ritva">Régime d’imposition à la TVA <sup class="text-danger">*</sup></label>
                                    <select id="ritva" class="form-control">
                                        <option selected="">Choisir...</option>
                                        <option>RN - Régime réel normal (mensuelle)</option>
                                        <option>T - Régime mini-réel (trimestrielle)</option>
                                        <option>RS - Régime réel simplifié (semestrielle)</option>
                                        <option>La franchise en base</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dc">Date de clôture du premier exercice</label>
                                    <input id="dc" type="text" class="form-control  date_premier_exercice" value="31 December 2020">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="dccopy">Date de clôture</label>
                                    <input id="dccopy" type="text" class="form-control date-picker date_cloture" value="31 December 2020">
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row mt-4">
                            <div class="col-12 my-3">
                                <label  class="weight-600">Adhésion CGA ? <sup class="text-danger">*</sup></label>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="oui4" name="ouinon4" class="custom-control-input">
                                    <label for="oui4" class="custom-control-label">Oui</label>
                                </div>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="non4" name="ouinon4" class="custom-control-input">
                                    <label for="non4" class="custom-control-label">Non</label>
                                </div>
                            </div>
                            <div id="divshow3" class="col-12 my-3">
                                <label  class="weight-600">Adhésion CGA faite ? <sup class="text-danger">*</sup></label>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="oui5" name="ouinon5" class="custom-control-input">
                                    <label for="oui5" class="custom-control-label">Oui</label>
                                </div>
                                <div class="custom-control custom-radio mb-5">
                                    <input type="radio" id="non5" name="ouinon5" class="custom-control-input">
                                    <label for="non5" class="custom-control-label">Non</label>
                                </div>
                            </div>

                            <div id="divshow6"class="col-md-6">
                                <div class="form-group">
                                    <label for="na">Numéro d'adhérent <sup class="text-danger">*</sup></label>
                                    <input id="na" type="text" class="form-control">
                                </div>
                            </div>
                            <div id="divshow4" class="col-md-6">
                                <div class="form-group">
                                    <label for="cga">Centre de gestion agrée <sup class="text-danger">*</sup></label>
                                    <select id="cga" class="form-control">
                                        <option selected="">Choisir...</option>
                                        <option>TERRA Gestion</option>
                                        <option>AGA picpus</option>
                                    </select>
                                </div>
                            </div>
                            <div  id="divshow5" class="col-md-12 columns download card mb-3">
                                <div class="card-body">
                                    <h5 class="card-title pl-1">Telecharger le bulletin d'adhésion</h5>
                                    <p class="card-text pl-1">Ce bulletin contient les informations saisies
                                        dans ce formulaire, merci de completer le reste des informations.</p><p>
                                        <a id="downloadpdf" href="vendors/images/bulletin.pdf" class="button m-2"
                                           target="_blank" ><i class="fa fa-download"></i>Télécharger le bulletin d’adhésion</a>
                                    </p>
                                </div>
                            </div>
                            <!-- <div id="divshow5" class="col-md-4 mt-4 pt-3 columns download">
                                <p>
                                    <a href="vendors/images/bulletin.pdf" class="button" target="_blank" ><i class="fa fa-download"></i>Télécharger le bulletin d’adhésion</a>
                                </p>
                            </div> -->

                        </div>
                    </section>
                    <!-- Step 4 -->
                    <h5>Gestion Adexca</h5>
                    <section>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ec2">Expert comptable <sup class="text-danger">*</sup></label>
                                    <select id="ec2" class="custom-select form-control">
                                        <option selected="">Choisir...</option>
                                        <option>AB - Aryé BISMUTH</option>
                                        <option>DB - Dylan BOUJENAH</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ca">Collaborateur assigné <sup class="text-danger">*</sup></label>
                                    <select id="ca" class="custom-select form-control">
                                        <option selected="">Choisir...</option>
                                        <option>Aryé</option>
                                        <option>Dylan</option>
                                        <option>Emanuella</option>
                                        <option>Maudline</option>
                                        <option>Dan</option>
                                        <option>Chinh</option>
                                        <option>Betsalel</option>
                                        <option>Mikael</option>
                                        <option>Sami</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr/>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="tc">Tarif Comptabilité </label>
                                        <input id="tc" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="form-group">
                                        <label for="tb">Tarif Bilan </label>
                                        <input id="tb" type="text" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="mr">Mode de règlement <sup class="text-danger">*</sup></label>
                                    <select id="mr" class="custom-select form-control">
                                        <option selected="">Choisir...</option>
                                        <option>Prélèvement bancaire</option>
                                        <option>Virement automatique</option>
                                        <option>Autre</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        </div>


        <!-- success Popup html Start -->
        <div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center font-18">
                        <h3 class="mb-20">Client enregistré!</h3>
                        <div class="mb-30 text-center"><img src="vendors/images/success.png"></div>
                        On peut mettre un texte ici
                    </div>
                    <div class="modal-footer justify-content-center">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">ok</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- success Popup html End -->

    </div>


<?php

include '../assets/layouts/footer.php'

?>
<script src="../src/plugins/jquery-steps/jquery.steps.js"></script>
<script src="../vendors/scripts/steps-setting.js"></script>
<script src="../vendors/scripts/countrySelect.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
<!-- switchery js -->
<script src="../src/plugins/switchery/switchery.min.js"></script>
<!-- bootstrap-tagsinput js -->
<script src="../src/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<!-- bootstrap-touchspin js -->
<script src="../src/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="../vendors/scripts/advanced-components.js"></script>
<!-- add sweet alert js & css in footer -->
<script src="../src/plugins/sweetalert2/sweetalert2.all.js"></script>
<script src="../src/plugins/sweetalert2/sweet-alert.init.js"></script>
<script>

    $(document).ready(function(){
        //Dès qu'on clique sur #oui1, on applique hide() au titre
        $("#oui1").click(function(){
            $("#divshow").hide();
        });

        //Dès qu'on clique sur #non1, on applique show() au titre
        $("#non1").click(function(){
            $("#divshow").show();
        });

        //Dès qu'on clique sur #qonto, on applique hide() au titre
        $("#qonto").click(function(){
            $("#mention").show();
        });

        //Dès qu'on clique sur #autre, on applique show() au titre
        $("#autre").click(function(){
            $("#mention").hide();
        });

        //Dès qu'on clique sur #autreact, on applique hide() au titre
        $("#autreact").click(function(){
            $("#nvact").show();
        });

        //Dès qu'on clique sur #seulact, on applique show() au titre
        $("#seulact").click(function(){
            $("#nvact").hide();
        });

        //Dès qu'on clique sur #oui2, on applique hide() au titre
        $("#oui2").click(function(){
            $("#divshow2").hide();
        });

        //Dès qu'on clique sur #non2, on applique show() au titre
        $("#non2").click(function(){
            $("#divshow2").show();
        });

        //Dès qu'on clique sur #oui1, on applique hide() au titre
        $("#oui4").click(function(){
            $("#divshow3").show();
        });
        //Dès qu'on clique sur #oui1, on applique hide() au titre
        $("#non4").click(function(){
            $("#divshow3").hide();
            $("#divshow4").hide();
            $("#divshow5").hide();
            $("#divshow6").hide();
        });

        //Dès qu'on clique sur #oui1, on applique hide() au titre
        $("#oui5").click(function(){
            $("#divshow5").hide();
            $("#divshow4").show();
            $("#divshow6").show();
        });
        //Dès qu'on clique sur #oui1, on applique hide() au titre
        $("#non5").click(function(){
            $("#divshow4").hide();
            $("#divshow6").hide();
            $("#divshow5").show();
        });
        $(".toggle-password").click(function() {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }
        });
        $(".countrypicker").countrySelect({
            presponsiveDropdown:true,
            defaultCountry:"fr"
        });


        var inputPhone0 = document.querySelector('#phone0'),
            errorMsg0 = document.querySelector("#error-msg0"),
            validMsg0 = document.querySelector("#valid-msg0");

        // here, the index maps to the error code returned from getValidationError - see readme
        var errorMap0 = ["Numéro invalide", "Code de pays non valide", "Trop court", "trop long", "Numéro invalide"];

        // initialise plugin
        var iti0 = window.intlTelInput(inputPhone0, {
            initialCountry:"fr",
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js"
        });

        var reset0 = function() {
            inputPhone0.classList.remove("error");
            errorMsg0.innerHTML = "";
            errorMsg0.classList.add("hide");
            validMsg0.classList.add("hide");
        };

        // on blur: validate
        inputPhone0.addEventListener('blur', function() {
            reset0();
            if (inputPhone0.value.trim()) {
                if (iti0.isValidNumber()) {
                    validMsg0.classList.remove("hide");
                } else {
                    inputPhone0.classList.add("error");
                    var errorCode = iti0.getValidationError();
                    errorMsg0.innerHTML = errorMap0[errorCode];
                    errorMsg0.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        inputPhone0.addEventListener('change', reset0);
        inputPhone0.addEventListener('keyup', reset0);


        var inputPhone1 = document.querySelector('#phone1'),
            errorMsg1 = document.querySelector("#error-msg1"),
            validMsg1 = document.querySelector("#valid-msg1");

        // here, the index maps to the error code returned from getValidationError - see readme
        var errorMap1 = ["Numéro invalide", "Code de pays non valide", "Trop court", "trop long", "Numéro invalide"];

        // initialise plugin
        var iti1 = window.intlTelInput(inputPhone1, {
            initialCountry:"fr",
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js"
        });

        var reset1 = function() {
            inputPhone1.classList.remove("error");
            errorMsg1.innerHTML = "";
            errorMsg1.classList.add("hide");
            validMsg1.classList.add("hide");
        };

        // on blur: validate
        inputPhone1.addEventListener('blur', function() {
            reset1();
            if (inputPhone1.value.trim()) {
                if (iti1.isValidNumber()) {
                    validMsg1.classList.remove("hide");
                } else {
                    inputPhone1.classList.add("error");
                    var errorCode = iti1.getValidationError();
                    errorMsg1.innerHTML = errorMap1[errorCode];
                    errorMsg1.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        inputPhone1.addEventListener('change', reset1);
        inputPhone1.addEventListener('keyup', reset1);


        // date de cloture du premier exercice
        $('.date_premier_exercice').datepicker({
            language: 'en',
            autoClose: true,
            dateFormat: 'dd MM yyyy',
        }).on('focusout', function(e) {
            e.preventDefault();
            var x = $(this).val();
            $('.date_cloture').val(x);
            console.log(x);
        });
    });
</script>
