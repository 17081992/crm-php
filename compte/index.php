<?php

define('TITLE', "compte");
include '../assets/layouts/header.php';
//check_verified();
function xss_filter($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="title">
                        <h4>Profil</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Profile</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">

                <div class="pd-20 card-box height-100-p">
                    <div class="profile-photo">
                        <img src="../assets/uploads/users/<?php echo $_SESSION['profile_image']; ?>" alt="..." width="400" class="mb-2 img-thumbnail">
                    </div>
                    <div class="profile-info">
                        <p class="small"><h5 class="text-center h5 mb-0">

                            <?php if ($_SESSION['gender'] == 'm'){ ?>

                                <i class="fa fa-male"></i>


                            <?php } elseif ($_SESSION['gender'] == 'f'){ ?>

                                <i class="fa fa-female"></i>

                            <?php } ?>

                        <?php echo $_SESSION['first_name'] . ' ' . $_SESSION['last_name']; ?>
                        </h5>
                        </p>

                        <p class="text-center text-muted font-14">
                            <?php echo $_SESSION['email']; ?>
                            <br/>
                            <?php echo $_SESSION['role']; ?>
                        </p>

                    </div>
                </div>

            </div>
            <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
                <div class="card-box height-100-p overflow-hidden">
                    <div class="profile-tab height-100-p">
                        <div class="tab height-100-p">
                            <ul class="nav nav-tabs customtab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#setting" role="tab">Paramètres du compte</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <!-- setting Tab start -->
                                <div class="tab-pane fade show active" id="setting" role="tabpanel">
                                    <div class="pd-20">
                                        <div class="profile-setting">
                                            <form class="form-auth" action="includes/profile-edit.inc.php" method="post" enctype="multipart/form-data" autocomplete="off">

                                                <?php insert_csrf_token(); ?>
                                                <ul class="profile-edit-list row">
                                                    <li class="col-md-6">
                                                        <h4 class="text-blue h5 mb-20">Modifier vos informations personnelles</h4>

                                                        <div class="form-group">
                                                            <label>Civilité</label>
                                                            <div class="d-flex">
                                                                <div class="custom-control custom-radio mb-5 mr-20">
                                                                    <input type="radio" id="male" name="gender" class="custom-control-input" value="m" <?php if ($_SESSION['gender'] == 'm') echo 'checked' ?>>
                                                                    <label class="custom-control-label weight-400" for="male">Homme</label>
                                                                </div>
                                                                <div class="custom-control custom-radio mb-5">
                                                                    <input type="radio" id="female" name="gender" class="custom-control-input" value="f" <?php if ($_SESSION['gender'] == 'f') echo 'checked' ?>>
                                                                    <label class="custom-control-label weight-400" for="female">Femme</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="ec2">Rôle <sup class="text-danger">*</sup></label>
                                                            <select id="ec2" class="custom-select form-control">
                                                                <option><?php echo $_SESSION['role']; ?></option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="first_name">Nom</label>
                                                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Nom" value="<?php echo xss_filter($_SESSION['first_name']); ?>">
                                                        </div>

                                                        <div class="form-group">
                                                            <label for="last_name">Prénom</label>
                                                            <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Prénom" value="<?php echo xss_filter($_SESSION['last_name']); ?>">
                                                        </div>


                                                    </li>
                                                    <li class="col-md-6">
                                                        <h4 class="text-blue h5 mb-20">Modifier vos accées</h4>
                                                        <div class="form-group">
                                                            <label for="email">Adresse Email</label>
                                                            <input type="email" id="email" name="email" class="form-control" placeholder="Email address" value="<?php echo xss_filter($_SESSION['email']); ?>">
                                                            <sub class="text-danger">
                                                                <?php
                                                                if (isset($_SESSION['ERRORS']['emailerror']))
                                                                    echo $_SESSION['ERRORS']['emailerror'];

                                                                ?>
                                                            </sub>
                                                        </div>
                                                        <span class="h5 font-weight-normal text-muted mb-4">Password Edit</span>
                                                        <br>
                                                        <sub class="text-danger">
                                                            <?php
                                                            if (isset($_SESSION['ERRORS']['passworderror']))
                                                                echo $_SESSION['ERRORS']['passworderror'];
                                                            ?>
                                                        </sub>
                                                        <br><br>
                                                        <div class="form-group">
                                                            <input id="currentpassword" type="password" class="form-control" name="password" placeholder="mot de passe actuel" autocomplete="new-password">
                                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>

                                                        </div>
                                                        <div class="form-group">
                                                            <input id="newpassword" type="password" class="form-control" name="newpassword" placeholder="nouveau mot de passe" autocomplete="new-password">
                                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <input id="confirmpassword" type="password" class="form-control" name="confirmpassword" placeholder="confirmez le nouveau mot de passe" autocomplete="new-password">
                                                            <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                                        </div>
                                                        <button class="btn btn-lg btn-primary btn-block mb-5" type="submit" name='update-profile'>Valider les modifications</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- setting Tab End -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php

include '../assets/layouts/footer.php'

?>
<script type="text/javascript">
    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#imagePreview').css('background-image', 'url(' + e.target.result + ')');
                $('#imagePreview').hide();
                $('#imagePreview').fadeIn(650);

            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#avatar").change(function() {
        console.log("here");
        readURL(this);
    });
    window.addEventListener('DOMContentLoaded', function () {

        $("body").on('click', '.toggle-password', function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $("#currentpassword");
            if (input.attr("type") === "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }

        });
        $("body").on('click', '.toggle-password', function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $("#newpassword");
            if (input.attr("type") === "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }

        });
        $("body").on('click', '.toggle-password', function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $("#confirmpassword");
            if (input.attr("type") === "password") {
                input.attr("type", "text");
            } else {
                input.attr("type", "password");
            }

        });
    });
</script>
<!-- buttons for Export datatable -->
<script src="../src/plugins/datatables/js/dataTables.buttons.min.js"></script>
<script src="../src/plugins/datatables/js/buttons.bootstrap4.min.js"></script>
<script src="../src/plugins/datatables/js/buttons.print.min.js"></script>
<script src="../src/plugins/datatables/js/buttons.html5.min.js"></script>
<script src="../src/plugins/datatables/js/buttons.flash.min.js"></script>
<script src="../src/plugins/datatables/js/pdfmake.min.js"></script>
<script src="../src/plugins/datatables/js/vfs_fonts.js"></script>
<!-- Datatable Setting js -->
<script src="../vendors/scripts/datatable-setting.js"></script>
<!-- chart Setting js -->
<script src="../src/plugins/highcharts-6.0.7/code/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="../src/plugins/highcharts-6.0.7/code/highcharts-more.js"></script>
<script src="../vendors/scripts/highchart-setting.js"></script>
