<?php

define('TITLE', "Nos prospects");
include '../assets/layouts/header.php';
//check_verified();

?>
<link rel="stylesheet" type="text/css" href="../src/plugins/datatables/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="../src/plugins/datatables/css/responsive.bootstrap4.min.css">

    <div class="min-height-200px">
        <div class="page-header">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="title">
                        <h4>Nos Prospects</h4>
                    </div>
                    <nav aria-label="breadcrumb" role="navigation">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="../home/">Accueil</a></li>
                            <li class="breadcrumb-item active" aria-current="page">Nos Prospects</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        <!-- tunnel de selection cl/pr start -->
        <div class="pd-20 mb-20 card-box">
            <div class="row m-2">
                <div class=" col-md-4 p-2">
                    <div class="card card-box p-1 text-center">
                        <div class="center-block"><img class="card-img-top" src="../vendors/images/img2.jpg" alt="Card image cap"></div>
                        <div class="card-body center-block">
                            <h5 class="card-title weight-500">L'entreprise est-elle déja crée ?</h5>
                            <p class="card-text">si l'entreprise n'est pas encore crée, nous allons crèer un nouveau prospect, sinon un nouveau client</p>
                            <a href="../nouveau-prospect/" class="btn btn-dark">Prospect</a>
                            <a href="../nouveau-client/" class="btn btn-outline-dark">Client</a>
                        </div>
                    </div>
                </div>
                <!-- tunnel de selection cl/pr end-->
                <!-- static chart start -->
                <div class="col-md-8 p-2">
                    <div class=" card card-box p-3">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div id="chart1"></div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- Static chart End-->
        <!-- Export Datatable start -->
        <div class="pd-20 mb-20 card-box">
            <div class="pd-20">
                <h4 class="text-blue h4">Liste des prospects</h4>
            </div>
            <div class="pb-20">
                <table id="table_data" class="data-table table table-responsive nowrap">
                    <thead>
                    <tr>
                        <th>Expert CP</th>
                        <th>Collab. CP</th>
                        <th>Forme juridique</th>
                        <th>Raison social</th>
                        <th>Représentant Légal</th>
                        <th>Numéro Tél.</th>
                        <th>Adresse mail</th>
                        <th>Date de création</th>
                        <th class="datatable-nosort">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>AB</td>
                        <td>Sami</td>
                        <td>SAS</td>
                        <td>wiwi dodo</td>
                        <td>qwerty hhh</td>
                        <td>+33 7 00 00 00 00</td>
                        <td>contact@wiwidodo.com</td>
                        <td>12/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>AB</td>
                        <td>Emanuella</td>
                        <td>SARL</td>
                        <td>bonjour</td>
                        <td>qqq ffff</td>
                        <td>+33 7 00 00 00 99</td>
                        <td>contact@bonjour.com</td>
                        <td>01/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>AB</td>
                        <td>Emanuella</td>
                        <td>SAS</td>
                        <td>Webimagic</td>
                        <td>Hechkel Imen</td>
                        <td>+33 6 29 30 18 98</td>
                        <td>contact@webimagic.com</td>
                        <td>05/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>AB</td>
                        <td>Dan</td>
                        <td>SA</td>
                        <td>digiweb</td>
                        <td>Mabrouk</td>
                        <td>+33 6 77 77 77 77</td>
                        <td>contact@webimagic.com</td>
                        <td>04/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>AB</td>
                        <td>Emanuella</td>
                        <td>SAS</td>
                        <td>dddd</td>
                        <td>Hechkel Imen</td>
                        <td>+33 6 29 30 18 98</td>
                        <td>contact@webimagic.com</td>
                        <td>04/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>AB</td>
                        <td>Emanuella</td>
                        <td>SAS</td>
                        <td>AAA aaaa</td>
                        <td>aaappp aaappp</td>
                        <td>+33 7 44 44 44 44</td>
                        <td>contact@aaa.com</td>
                        <td>11/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>DB</td>
                        <td>chinh</td>
                        <td>SARL</td>
                        <td>kitchen co</td>
                        <td>Hechkel Imen</td>
                        <td>+33 6 44 44 44 44</td>
                        <td>contact@kitchenco.com</td>
                        <td>14/11/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>AB</td>
                        <td>Emanuella</td>
                        <td>EURL</td>
                        <td>tunisino</td>
                        <td>qhdhdh iiii</td>
                        <td>+33 6 11 11 11 11</td>
                        <td>contact@tunisino.com</td>
                        <td>25/11/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>AB</td>
                        <td>Emanuella</td>
                        <td>SAS</td>
                        <td>digitalgo</td>
                        <td>Hechkel dedou</td>
                        <td>+33 6 77 77 77 77</td>
                        <td>contact@digitalgo.com</td>
                        <td>25/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>DB</td>
                        <td>Mikeal</td>
                        <td>EIRL</td>
                        <td>goprint</td>
                        <td>azerty uio</td>
                        <td>+33 6 99 99 99 99</td>
                        <td>contact@goprint.com</td>
                        <td>17/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>DB</td>
                        <td>Chinh</td>
                        <td>SARL</td>
                        <td>Hello world</td>
                        <td>Didou yoyoyo</td>
                        <td>+33 11 11 11 11</td>
                        <td>contact@helloworld</td>
                        <td>05/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>AB</td>
                        <td>Dan</td>
                        <td>SAS</td>
                        <td>Widocode</td>
                        <td>Mecheri Walid</td>
                        <td>+33 6 00 00 11 11</td>
                        <td>contact@Widocode.com</td>
                        <td>07/12/2020</td>
                        <td>
                            <div class="dropdown">
                                <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                    <i class="dw dw-more"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                    <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> Afficher</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Modifier</a>
                                    <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Supprimer</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th>Expert CP</th>
                        <th>Collab. CP</th>
                        <th>Forme juridique</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>





<?php

include '../assets/layouts/footer.php'

?>

<!-- Datatable Setting js -->
<script src="../vendors/scripts/datatable-setting.js"></script>
<!-- chart Setting js -->
<script src="../src/plugins/highcharts-6.0.7/code/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="../src/plugins/highcharts-6.0.7/code/highcharts-more.js"></script>
<script src="../vendors/scripts/highchart-setting.js"></script>
