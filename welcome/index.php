<?php

define('TITLE', "Welcome");
include '../assets/layouts/header.php';

?>



<main role="main">

    <section class="jumbotron text-center py-5">
        <div class="container">
            <h1 class="jumbotron-heading mb-4">PHP Login System</h1>
            <p class="text-muted">
                Embeddable and Secure Authentication System in PHP with User Profiles, Profile Editing, Login, Signup, Account Verification via 
                Email, Password Reset System and Remember Me Feature.
                
                <hr width="300" class="my-3">

                <sub>
                    This is an example intro page that can be accessed with or without logging into an account. This may be used as a default homepage
                    for the application with basic intro information.
                </sub>

                <hr width="300" class="my-3">

                <sub>
                    Hey if you like this small project, I would like it best if you decide to improve and contribute to it. We are probably
                    too far apart for me to demand a nice coffee but for now a star would suffice.
                </sub>
            </p>
             
        </div>
    </section>


</main>


<?php

include '../assets/layouts/footer.php'

?>